//
//  CacheDataService.m
//  Marvel Characters
//
//  Created by Krazyzy on 10.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import "CacheDataService.h"
#import "Character.h"

@implementation CacheDataService

-(void)writeFile:(NSDictionary*)dicToWrite{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"saved_dic"];
    
    [dicToWrite writeToFile:filePath atomically:YES];
}

-(NSArray*)readFile{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"saved_dic"];
    
    NSDictionary *jsonData = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    NSDictionary *data     = [jsonData objectForKey:@"data"];
    NSArray      *results  = [data objectForKey:@"results"];
    
    NSMutableArray *characters = [[NSMutableArray alloc] init];
    

    for (NSDictionary *dic in results) {
        
        NSString *comicsURL = [[dic objectForKey:@"comics"] objectForKey:@"collectionURI"];
        
        NSDictionary *thumbnail = [dic objectForKey:@"thumbnail"];
        NSURL *thumbnailURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@.%@", [thumbnail objectForKey:@"path"], [thumbnail objectForKey:@"extension"]]];
        
        [characters addObject:[[Character alloc] initWithName:[dic objectForKey:@"name"]
                                                         info:[dic objectForKey:@"description"]
                                                    comicsURL:comicsURL
                                                 thumbnailURL:thumbnailURL]];
    }

    return [characters autorelease];
}

@end
