//
//  CharacterTVCTableViewCell.h
//  Marvel Characters
//
//  Created by Krazyzy on 09.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CharacterTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *info;

@end
