//
//  Character.m
//  Marvel Characters
//
//  Created by Krazyzy on 09.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import "Character.h"

@implementation Character

-(id)initWithName:(NSString*)name info:(NSString*)info comicsURL:(NSString*)comicsURL thumbnailURL:(NSURL*)thumbnailURL{
    
    self = [super init];
        if (self) {
            self.name         = name;
            self.info         = info;
            self.comicsURL    = comicsURL;
            self.thumbnailURL = thumbnailURL;
        }
    
    return self;
}

@end
