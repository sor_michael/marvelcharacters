//
//  MarvelService.m
//  Marvel Characters
//
//  Created by Krazyzy on 09.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import "MarvelService.h"
#import "AFNetworking.h"
#import "NSString+MD5.h"
#import "Character.h"
#import "Comics.h"
#import "CacheDataService.h"

@implementation MarvelService

static NSString *const publicKey  = @"83774223a3032fa8f7ec856a39fae515";
static NSString *const privateKey = @"2fc9b0d0c9c44d47521778a5382c7e0c71dbdd61";


-(void)getCharactersWithOffset:(int)offset{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    NSInteger   timeStampInt = round(timeStamp);
    NSString           *hash = [[NSString stringWithFormat:@"%ld%@%@", (long)timeStampInt, privateKey, publicKey] MD5String];
    
    NSDictionary *params = @{ @"ts"    :[NSString stringWithFormat:@"%ld", (long)timeStampInt],
                              @"apikey":publicKey,
                              @"hash"  :hash,
                              @"offset":[NSString stringWithFormat:@"%d", offset] };
    
    [manager GET:@"https://gateway.marvel.com:443/v1/public/characters" parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
    
        NSDictionary *jsonDict = (NSDictionary *) responseObject;
        
        if(offset == 0){
            CacheDataService *cacheSrv = [[CacheDataService alloc] init];
            [cacheSrv writeFile:jsonDict];
        }
        
        NSDictionary *data     = [jsonDict objectForKey:@"data"];
        NSArray      *results  = [data objectForKey:@"results"];
        
        NSMutableArray *characters = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dic in results) {
            
            NSString *comicsURL = [[dic objectForKey:@"comics"] objectForKey:@"collectionURI"];
            
            NSDictionary *thumbnail = [dic objectForKey:@"thumbnail"];
            NSURL *thumbnailURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@.%@", [thumbnail objectForKey:@"path"], [thumbnail objectForKey:@"extension"]]];
            
            [characters addObject:[[Character alloc] initWithName:[dic objectForKey:@"name"]
                                                             info:[dic objectForKey:@"description"]
                                                        comicsURL:comicsURL
                                                     thumbnailURL:thumbnailURL]];
        }
        
        [self.delegate didUpdeteCharacters: [characters autorelease]];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        [self.delegate updateError:error.localizedDescription];
    }];
}

-(void)getComics:(NSString*)comicsURL WithOffset:(int)offset{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
    [serializer setRemovesKeysWithNullValues:YES];
    [manager setResponseSerializer:serializer];
    
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    NSInteger   timeStampInt = round(timeStamp);
    NSString           *hash = [[NSString stringWithFormat:@"%ld%@%@", (long)timeStampInt, privateKey, publicKey] MD5String];
    
    NSDictionary *params = @{ @"ts"    :[NSString stringWithFormat:@"%ld", (long)timeStampInt],
                              @"apikey":publicKey,
                              @"hash"  :hash,
                              @"offset":[NSString stringWithFormat:@"%d", offset] };
    
    [manager GET:comicsURL parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSDictionary *jsonDict = (NSDictionary *) responseObject;
        NSDictionary *data     = [jsonDict objectForKey:@"data"];
        NSArray      *results  = [data objectForKey:@"results"];
        
        NSMutableArray *comics = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dic in results) {
            
            NSDictionary *thumbnail = [dic objectForKey:@"thumbnail"];
            NSURL *thumbnailURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@.%@", [thumbnail objectForKey:@"path"], [thumbnail objectForKey:@"extension"]]];
            
            [comics addObject:[[Comics alloc] initWithName:[dic objectForKey:@"title"]
                                                      info:[dic objectForKey:@"description"]
                                              thumbnailURL:thumbnailURL]];
        }

        [self.delegate didUpdeteComics:[comics autorelease]];
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        [self.delegate updateError:error.localizedDescription];
    }];
}

@end
