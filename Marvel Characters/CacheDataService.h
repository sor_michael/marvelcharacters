//
//  CacheDataService.h
//  Marvel Characters
//
//  Created by Krazyzy on 10.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheDataService : NSObject

-(void)writeFile:(NSDictionary*)dicToWrite;
-(NSArray*)readFile;

@end
