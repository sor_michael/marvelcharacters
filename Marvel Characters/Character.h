//
//  Character.h
//  Marvel Characters
//
//  Created by Krazyzy on 09.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Character : NSObject

@property (nonatomic, retain)  NSString *name;
@property (nonatomic, retain)  NSString *info;
@property (nonatomic, retain)  NSString *comicsURL;
@property (nonatomic, retain)  NSURL    *thumbnailURL;


-(id)initWithName:(NSString*)name info:(NSString*)info comicsURL:(NSString*)comicsURL thumbnailURL:(NSURL*)thumbnailURL;


@end
