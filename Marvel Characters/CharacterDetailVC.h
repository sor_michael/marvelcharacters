//
//  CharacterDetailVC.h
//  Marvel Characters
//
//  Created by Krazyzy on 08.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarvelService.h"

@interface CharacterDetailVC : UICollectionViewController<UICollectionViewDelegate, UICollectionViewDataSource, RequestCompleteProtocol>

@property (nonatomic, retain)  NSString *comicsURL;

@end
