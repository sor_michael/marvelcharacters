//
//  CharacterDetailVC.m
//  Marvel Characters
//
//  Created by Krazyzy on 08.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import "CharacterDetailVC.h"
#import "ComicsCVC.h"
#import "Comics.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CharacterDetailVC ()

@end

@implementation CharacterDetailVC{
    NSMutableArray *comicsArr;
    MarvelService *service;
    UIRefreshControl *refreshControl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    comicsArr = [[NSMutableArray alloc] init];
    
    service = [[MarvelService alloc] init];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor redColor];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self action:@selector(refreshCollection) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    self.collectionView.alwaysBounceVertical = YES;

    [self refreshCollection];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [refreshControl beginRefreshing];
    [self.collectionView setContentOffset:CGPointMake(0, -refreshControl.frame.size.height) animated:YES];
    
}

-(void)refreshCollection{
    [self clearCollectionView];
    service.delegate = self;
    [service getComics:self.comicsURL WithOffset:0];
}

-(void)clearCollectionView{
    [comicsArr removeAllObjects];
    [self.collectionView reloadData];
}

-(void)didUpdeteComics:(NSArray *)comics{
    [comicsArr addObjectsFromArray:comics];
    [self.collectionView reloadData];
    [refreshControl endRefreshing];
    service.delegate = nil;
}

-(void)updateError:(NSString *)errorMsg{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:errorMsg
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:^(){
        [refreshControl endRefreshing];
        service.delegate = nil;
    }];
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return comicsArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ComicsCVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"comicsCell" forIndexPath:indexPath];
    
    Comics *someComics = [comicsArr objectAtIndex:indexPath.row];
    cell.title.text = someComics.title;
    cell.info.text  = someComics.info;
    [cell.thumbnail sd_setImageWithURL:someComics.thumbnailURL  placeholderImage:[UIImage imageNamed:@"Marvel-logo"]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == [comicsArr count] - 1) {
        service.delegate = self;
        [service getComics:self.comicsURL WithOffset:(int)comicsArr.count];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        Comics *someComics = [comicsArr objectAtIndex:indexPath.row];
        
        CGFloat titleHeight = [self findHeightForText:someComics.title havingMaximumWidth:180 andFont:[UIFont systemFontOfSize:20]];
        CGFloat infoHeight = [self findHeightForText:someComics.info havingMaximumWidth:180 andFont:[UIFont systemFontOfSize:12]];
        return CGSizeMake(180, 344 + titleHeight + infoHeight);
    }
    else {
        return CGSizeMake((([UIScreen mainScreen].bounds.size.width - 2.0) / 3.0), (([UIScreen mainScreen].bounds.size.width - 4.0) / 3.0));
    }
}

- (CGFloat)findHeightForText:(NSString *)text havingMaximumWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
    if (text) {
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size.height;
}




@end
