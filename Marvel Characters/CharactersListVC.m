//
//  CharactersListVC.m
//  Marvel Characters
//
//  Created by Krazyzy on 08.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import "CharactersListVC.h"
#import "CharacterTVC.h"
#import "Character.h"
#import "CharacterDetailVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CacheDataService.h"

@implementation CharactersListVC{
    NSMutableArray *chars;
    MarvelService *service;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    chars = [[NSMutableArray alloc] init];
    
    service = [[MarvelService alloc] init];

    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor redColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(refreshFeed)
                  forControlEvents:UIControlEventValueChanged];
    [self.refreshControl beginRefreshing];
    [self.tableView setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
    [self checkCache];

}

-(void)checkCache{
    [self clearTableView];
    CacheDataService *cacheSrv = [[CacheDataService alloc] init];
    [chars addObjectsFromArray:[cacheSrv readFile]];
    
    if(chars.count != 0) {
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
    } else{
        [self refreshFeed];
    }
    [cacheSrv release];
}

-(void)refreshFeed{
    service.delegate = self;
    [self clearTableView];
    [service getCharactersWithOffset:0];
}

-(void)clearTableView{
    [chars removeAllObjects];
    [self.tableView reloadData];
}

-(void)didUpdeteCharacters:(NSArray*)characters{
    [chars addObjectsFromArray:characters];
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
    service.delegate = nil;
}

-(void)updateError:(NSString *)errorMsg{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:errorMsg
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:^(){
        [self.refreshControl endRefreshing];
        service.delegate = nil;
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return chars.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CharacterTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"characterCell" forIndexPath:indexPath];
    
    Character *someChar = [chars objectAtIndex:indexPath.row];
    cell.name.text = someChar.name;
    cell.info.text = someChar.info;
    [cell.thumbnail sd_setImageWithURL:someChar.thumbnailURL  placeholderImage:[UIImage imageNamed:@"Marvel-logo"]];

    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"charComicsSeg"]) {
        
        CharacterDetailVC *vc = segue.destinationViewController;

        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Character *someChar = [chars objectAtIndex:indexPath.row];
        vc.comicsURL =  someChar.comicsURL;
        
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == [chars count] - 1) {
        service.delegate = self;
        [service getCharactersWithOffset:(int)chars.count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Character *someChar = [chars objectAtIndex:indexPath.row];
    
    return 80 + [self heightForText:someChar.info];
    
}

-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    CGFloat hotizontalPadding = 0;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - hotizontalPadding, MAX_HEIGHT)];
    textView.text = text;
    textView.font = [UIFont boldSystemFontOfSize:12];
    [textView sizeToFit];
    return textView.frame.size.height;
}

@end
