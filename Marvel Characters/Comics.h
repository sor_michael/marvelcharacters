//
//  Comics.h
//  Marvel Characters
//
//  Created by Krazyzy on 09.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comics : NSObject

@property (nonatomic, retain)  NSString *title;
@property (nonatomic, retain)  NSString *info;
@property (nonatomic, retain)  NSURL    *thumbnailURL;


-(id)initWithName:(NSString*)title info:(NSString*)info thumbnailURL:(NSURL*)thumbnailURL;

@end
