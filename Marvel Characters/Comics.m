//
//  Comics.m
//  Marvel Characters
//
//  Created by Krazyzy on 09.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import "Comics.h"

@implementation Comics

-(id)initWithName:(NSString*)title info:(NSString*)info thumbnailURL:(NSURL*)thumbnailURL{
    
    self = [super init];
    if (self) {
        self.title        = title;
        self.info         = info;
        self.thumbnailURL = thumbnailURL;
    }
    
    return self;
}

@end
