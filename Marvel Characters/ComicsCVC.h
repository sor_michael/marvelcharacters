//
//  ComicsCVC.h
//  Marvel Characters
//
//  Created by Krazyzy on 10.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComicsCVC : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *info;

@end
