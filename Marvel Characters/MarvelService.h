//
//  MarvelService.h
//  Marvel Characters
//
//  Created by Krazyzy on 09.09.17.
//  Copyright © 2017 Michael Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RequestCompleteProtocol<NSObject>

@optional
-(void)didUpdeteCharacters:(NSArray*)characters;
-(void)didUpdeteComics:(NSArray*)comics;
-(void)updateError:(NSString*)errorMsg;

@end

@interface MarvelService : NSObject

@property(nonatomic,strong) id delegate;

-(void)getCharactersWithOffset:(int)offset;
-(void)getComics:(NSString*)comicsURL WithOffset:(int)offset;

@end
